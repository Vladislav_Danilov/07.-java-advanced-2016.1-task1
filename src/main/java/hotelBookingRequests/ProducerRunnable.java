package hotelBookingRequests;

public class ProducerRunnable extends Thread {
  public MyBlockingQueue myBlockingQueue;
  public HotelBookingRequests hotelBookingRequests;



  public ProducerRunnable(MyBlockingQueue myBlockingQueue,
      HotelBookingRequests hotelBookingRequests) {
    super();
    this.myBlockingQueue = myBlockingQueue;
    this.hotelBookingRequests = hotelBookingRequests;
  }



  @Override
  public void run() {
    try {
      myBlockingQueue.put(hotelBookingRequests, this.getName());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
