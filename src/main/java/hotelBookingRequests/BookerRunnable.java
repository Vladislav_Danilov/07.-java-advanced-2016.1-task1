package hotelBookingRequests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookerRunnable extends Thread {
  Logger log = LoggerFactory.getLogger(BookerRunnable.class);
  MyBlockingQueue myBlockingQueue;
  HotelBookingRequests hotelBookingRequests;


  public BookerRunnable(MyBlockingQueue myBlockingQueue) {
    super();
    this.myBlockingQueue = myBlockingQueue;
  }


  @Override
  public void run() {
    try {
      hotelBookingRequests = myBlockingQueue.take(this.getName());
      log.info("booker " + this.getName() + ": processed " + hotelBookingRequests.idBokking);
      sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
