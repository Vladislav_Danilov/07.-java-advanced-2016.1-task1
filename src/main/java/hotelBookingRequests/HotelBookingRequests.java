package hotelBookingRequests;

import java.time.LocalDate;

public class HotelBookingRequests {

  public Integer idBokking;
  public LocalDate dataBooking;
  public String nameHotelBooking;
  public Integer numberRoomBooking;
  
  public HotelBookingRequests(Integer idBokking, LocalDate dataBooking,
      String nameHotelBooking, Integer numberRoomBooking) {
    super();
    this.idBokking = idBokking;
    this.dataBooking = dataBooking;
    this.nameHotelBooking = nameHotelBooking;
    this.numberRoomBooking = numberRoomBooking;
  }


}
