package hotelBookingRequests;

import java.time.LocalDate;
import java.time.Month;
import java.util.Random;

public class Main {

  public static void main(String[] args) {
    MyBlockingQueue myBlockingQueue = new MyBlockingQueue(3);
    HotelBookingRequests request1 =
        new HotelBookingRequests(333, LocalDate.of(2014, Month.DECEMBER, 12), "����� 1", 13);
    HotelBookingRequests request2 =
        new HotelBookingRequests(666, LocalDate.of(2014, Month.DECEMBER, 12), "����� 1", 36);
    HotelBookingRequests request3 =
        new HotelBookingRequests(999, LocalDate.of(2014, Month.DECEMBER, 12), "����� 1", 99);
    ProducerRunnable producer1 = new ProducerRunnable(myBlockingQueue, request1);
    producer1.setName("producer1");
    ProducerRunnable producer2 = new ProducerRunnable(myBlockingQueue, request2);
    producer2.setName("producer2");
    ProducerRunnable producer3 = new ProducerRunnable(myBlockingQueue, request3);
    producer3.setName("producer3");
    BookerRunnable booker1 = new BookerRunnable(myBlockingQueue);
    booker1.setName("booker1");
    BookerRunnable booker2 = new BookerRunnable(myBlockingQueue);
    booker2.setName("booker2");
    BookerRunnable booker3 = new BookerRunnable(myBlockingQueue);
    booker3.setName("booker3");
    BookerRunnable booker4 = new BookerRunnable(myBlockingQueue);
    booker4.setName("booker4");
    BookerRunnable booker5 = new BookerRunnable(myBlockingQueue);
    booker5.setName("booker5");
    BookerRunnable booker6 = new BookerRunnable(myBlockingQueue);
    booker6.setName("booker6");
    ProducerRunnable[] listProducerRunnable = {producer1, producer2, producer3};
    Random random = new Random();
    for (int i = 0; i < 16; i++) {
      new Thread(listProducerRunnable[random.nextInt(3)]).start();
      new Thread(booker1).start();
      new Thread(booker2).start();
      new Thread(booker3).start();
      new Thread(booker4).start();
      new Thread(booker5).start();
      new Thread(booker6).start();


    }
  }

}
