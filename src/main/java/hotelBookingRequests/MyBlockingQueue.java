package hotelBookingRequests;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyBlockingQueue {
  Logger log = LoggerFactory.getLogger(BookerRunnable.class);
  public List<HotelBookingRequests> queue = new LinkedList<HotelBookingRequests>();
  public int limit = 10;

  public MyBlockingQueue(int limit) {
    this.limit = limit;
  }


  public synchronized void put(HotelBookingRequests hotelBookingRequests,
      String nameProducer) throws InterruptedException {
    while (this.queue.size() == this.limit) {
      wait();
    }
    if (this.queue.size() == 0) {
      notifyAll();
    }

    this.queue.add(hotelBookingRequests);
    log.info("producer " + nameProducer + ": sent " + hotelBookingRequests.idBokking);
  }


  public synchronized HotelBookingRequests take(String nameBooker)
      throws InterruptedException {
    while (this.queue.size() == 0) {
      wait();
    }
    if (this.queue.size() == this.limit) {
      notifyAll();
    }
    log.info("booker " + nameBooker + ": received " + queue.get(0).idBokking);
    return this.queue.remove(0);
  }
}
